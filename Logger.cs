﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class Logger
    {
        private string type;
        private string filePath;

        public Logger(string type, string filepath)
        {
            this.type = type;
            this.filePath = filepath;
        }
        public void Log(string message)
        {
            if (this.type.Equals("Console"))
                Console.WriteLine(message);
            else if (this.type.Equals("File"))
                using (System.IO.StreamWriter writter =
                    new System.IO.StreamWriter(this.filePath))
                { writter.WriteLine(message); }
            else
                throw new ArgumentException("Unknown type");
        }
    }
}