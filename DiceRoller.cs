﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    //class DiceRoller
    //{

    //    private List<Die> dice;
    //    private List<int> resultForEachRoll;

    //    public DiceRoller()
    //    {
    //        this.dice = new List<Die>();
    //        this.resultForEachRoll = new List<int>();
    //    }

    //    public void InsertDie(Die die)
    //    {
    //        dice.Add(die);
    //    }

    //    public void RollAllDice()
    //    {
    //        resultForEachRoll.Clear();
    //        foreach (Die die in dice)
    //        {
    //            resultForEachRoll.Add(die.Roll());
    //        }
    //    }
    //}
    class DiceRoller : ILogable, IDiceRoller
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private Ilogger logger;
        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            this.ChooseLogger(new ConsoleLogger());
        }
        public void ChooseLogger(Ilogger logger)
        {
            this.logger = logger;
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.
                ReadOnlyCollection<int>(this.resultForEachRoll);
        }
        //4
        //public void LogRollingResults()
        //{
        //    foreach (int result in this.resultForEachRoll)
        //    {
        //        logger.Log(result.ToString());
        //    }
        //}
        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (int rollResult in resultForEachRoll)
            {
                stringBuilder.Append(rollResult).Append("\n");
            }
            return stringBuilder.ToString();
        }
        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }

    }
}
 