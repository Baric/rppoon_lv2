﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class ClosedDiceRoller : IDiceRollerRoll, ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;

        public ClosedDiceRoller(int numberOfDice, int numberOfSides)
        {
            dice = new List<Die>();
            resultForEachRoll = new List<int>();
            for (int i = 0; i < numberOfDice; i++)
            {
                dice.Add(new Die(numberOfSides));
            }
        }

        public void InsertDie(Die die)
        {
            throw new NotImplementedException();
        }

        public void RemoveAllDice()
        {
            throw new NotImplementedException();
        }

        public void RollAllDice()
        {
            resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                resultForEachRoll.Add(die.Roll());
            }
        }
        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (int rollResult in resultForEachRoll)
            {
                stringBuilder.Append(rollResult).Append("\n");
            }
            return stringBuilder.ToString();
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.
                ReadOnlyCollection<int>(this.resultForEachRoll);
        }
    }
}