﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_RPPOON
{
    //primjer1
    class Die
    {
        private int numberOfSides;

        //private Random randomGenerator;

        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}
        //zad2
        //public Die(int numberOfSides, Random random)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = random;
        //}

        //public int Roll()
        //{
        //    int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
        //    return rolledNumber;
        //}
        //zadatak3
        private RandomGenerator randomGenerator;

        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }

        //zadatak7
        public int getNumberOfSides()
        {
            return this.numberOfSides;
        }
    }
}
