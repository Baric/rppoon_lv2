﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2_RPPOON
{
    class FlexibleDiceRoller: IClosedDiceRoller,IFlexibleDiceRoller
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;

        public FlexibleDiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }

        public void InsertDie(Die die)
        {
            dice.Add(die);
        }

        public void RemoveAllDice()
        {
            this.dice.Clear();
            this.resultForEachRoll.Clear();
        }

        public void RollAllDice()
        {
            this.resultForEachRoll.Clear();
            foreach(Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        //zadatak7
        public void RemoveSameAsGiven(int value)
        {
            for(int i=0; i<dice.Count(); i++)
            {
                if(dice[i].getNumberOfSides() == value)
                {
                    dice.RemoveAt(i);
                    i--;
                }
            }
        }

        public void PrintDices()
        {
            foreach(Die die in dice)
            {
                Console.WriteLine("Die: " + die.getNumberOfSides());
            }
        }
    }
}
