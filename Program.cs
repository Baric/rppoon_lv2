﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV2
{   


//class Program
//{
//	static void Main(string[] args)
//	{
//		DiceRoller diceRoller = new DiceRoller();
//		Random randomGenerator = new Random();

//		FillWithDice(diceRoller);
//		diceRoller.RollAllDice();
//		PrintRollResults(diceRoller.GetRollingResults());
//	}

//	private static DiceRoller FillWithDice(DiceRoller diceRoller)
//	{
//		for(int i = 0; i < 10; i++)
//		{
//			diceRoller.InsertDie(new Die(6)); //1, 3
//			//diceRoller.InsertDie(new Die(6, randomGenerator)); //2
//		}
//		return diceRoller;
//	}

//	private static void PrintRollResults(IList<int> rollResults)
//	{
//		foreach(int r in rollResults)
//		{
//			Console.Write(r + "");
//		}
//		Console.WriteLine();
//	}
//}


	//	4
	//	Console.WriteLine("4: -> default(console)");
	//	DiceRoller RollerNum4 = new DiceRoller();
	//	for (int i = 0; i< 20; i++)
	//	{
	//	    Die die4 = new Die(6);
	//	RollerNum4.InsertDie(die4);
	//	}
	//RollerNum4.RollAllDice();
	//	//printResults(RollerNum4);
	//	RollerNum4.LogRollingResults();

	//Console.WriteLine("Four: -> fileLogger(filepath)");
	//FileLogger loger = new FileLogger("C:\\Users\\Matija\\source\\repos\\LV2_RPPOON\\LV2_RPPOON\\text.txt");
	//DiceRoller RollerNum5 = new DiceRoller(loger);
	//for (int i = 0; i < 20; i++)
	//{
	//    Die die5 = new Die(6);
	//    RollerNum5.InsertDie(die5);
	//}
	//RollerNum5.RollAllDice();
	//printResults(RollerNum5);
	//RollerNum5.LogRollingResults();

	class Program
	{
		static void Main(string[] args)
		{
			DiceRoller diceRoller = new DiceRoller();
			ConsoleLogger logger = new ConsoleLogger();

			FillWithDice(diceRoller);
			diceRoller.RollAllDice();
			logger.Log(diceRoller);
		}

		private static DiceRoller FillWithDice(DiceRoller diceRoller)
		{
			for (int i = 0; i < 10; i++)
			{
				diceRoller.InsertDie(new Die(6));
			}
			return diceRoller;
		}


	}
}