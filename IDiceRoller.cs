﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    interface IDiceRoller
    {
        void InsertDie(Die die);
        void RemoveAllDice();
        void RollAllDice();
    }
}